# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 13:45:36 2019

@author: dletran
"""

import os
import pandas as pd
from sqlalchemy import create_engine
import sqlalchemy as sql

#connect to database
#db: hello_world
#pw: <YOU-NEED-TO-CHANGE-THIS>
     
def main():
    engine = create_engine('postgresql://mimic_ro:password@psql-mimic.ri.ucdavis.edu:5432/mimic')

    #how to create a new schema
    engine.execute(sql.schema.CreateSchema('duke'))
    engine.execute(sql.schema.CreateSchema('duke2-practice'))
           
    # grab list of files
    ls_files = [x for x in os.listdir() if ('.pdf' not in x)]

    # test
    df = pd.read_csv(ls_files[0])
    df.to_sql(ls_files[0], engine, schema='duke')

    # iterate through ls of csv files; load into database
    for csv_file_name in ls_files:
        df = None; df = pd.DataFrame() #flush
        try:# regular UTF-8 encoding
            df = pd.read_csv(csv_file_name, low_memory=False) 
        except UnicodeDecodeError: # not UTF-8 encoding for some reason
            df = pd.read_csv( csv_file_name, low_memory=False, encoding="ISO-8859-1") 
        except Exception as e: # print errors
            print("Unexpected Error; " + csv_file_name, e)
            continue
        df.columns = [cols.lower() for cols in df.columns] #postgres doesn't like capitals or spaces
        df.to_sql(csv_file_name, engine, schema='duke', if_exists='replace')

if __name__ == '__main__':
       main()
# Fin
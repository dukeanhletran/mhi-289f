-- Author: Duke LeTran
-- Date: 2019-03-20
-- Description: Upload of smaller table via SQL code
-- Source: https://docs.google.com/spreadsheets/d/1SPtNfaO2SuedcJC2Zl_78qbnQJm9b1vJgrGj9goc6Uk/edit?usp=sharing
--

SET search_path TO 'duke';

-- # I. BridgeTable | brgNutrientsToAvoid
CREATE TABLE "brgCancer2NutrientsToAvoid" (
  cancer_type_id INT NOT NULL,
  "FoodTypes" VARCHAR(30) NOT NULL,
  "FoodToAvoid" VARCHAR
);

INSERT INTO "brgCancer2NutrientsToAvoid" (
	cancer_type_id, "NutrientsToAvoid") 
VALUES 
    (7,'Carotene, beta'),
    (7,'Vitamin A, IU'),
    (7,'Phosphorus, P')
    (7,'Acrylamide');


-- # II. BridgeTable | brgFoodsToAvoid
CREATE TABLE "brgCancer2FoodsToAvoid" (
  cancer_type_id INT NOT NULL,
  "FoodsTypes" VARCHAR(30) NOT NULL,
  "FoodsToAvoid" VARCHAR(30)
);

INSERT INTO "brgCancer2FoodsToAvoid" (
	cancer_type_id, "FoodsTypes", "FoodsToAvoid")
VALUES
	(2,'Red Meat','BEEF'),
	(2,'Red Meat','VEAL'),
	(2,'Red Meat','DEER'),
	(2,'Red Meat','LAMB'),
	(2,'Red Meat','GOAT'),
	(2,'Red Meat','PORK'),
	(2,'Red Meat','MUTTON'),
	(2,'Red Meat','HORSE'),
	(2,'Processed Meat','HOT DOG'),
	(2,'Processed Meat','HAM'),
	(2,'Processed Meat','BACON'),
	(2,'Processed Meat','SAUSAGE'),
	(2,'Processed Meat','DELI'),
	(2,'Processed Meat','FRANKFURTERS'),
	(2,'Processed Meat','CORNED BEEF'),
	(2,'Processed Meat','BEEF JERKY'),
	(2,'Processed Meat','PEPPERONI');


-- # III. BridgeTable | brgFoodsToEat
CREATE TABLE "brgCancer2FoodsToEat" (
  cancer_type_id INT NOT NULL,
  "FoodsToEat" VARCHAR(30) NOT NULL
);

INSERT INTO "brgCancer2FoodsToEat" (
	cancer_type_id, "FoodsToEat") 
VALUES 
	(2,'FISH'),
	(2,'VEGETABLES'),
	(2,'VEGGIES'),
	(2,'GREENS'),
	(2,'BEANS'),
	(2,'FRUITS'),
	(2,'WHOLE GRAIN');

-- # IV. Create a tblNutrients
CREATE TABLE "tblNutrients" AS (
	SELECT DISTINCT nutrient_code, nutrient_name
	FROM "brgNutrients2Foods"
	ORDER BY nutrient_code);


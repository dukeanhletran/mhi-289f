﻿-- Author: Duke LeTran
-- Date: 2019-03-20
-- Description: Create interesting Materialized Views

SET search_path TO 'duke';

-- beta carotene and vitamin A together are known to increase risks of Lung cancer
-- # I. create VIEW of FoodProducts with beta carotene and vitamin A
CREATE MATERIALIZED VIEW vwFoodsWithBCaroteneAndVitA AS(
	SELECT * FROM "tblBrandedFoodProducts"
	WHERE ndb_number IN(
		SELECT T1.ndb_no FROM
			(SELECT ndb_no FROM "tblNutrients" WHERE nutrient_name = 'Carotene, beta') AS T1
		INNER JOIN
			(SELECT ndb_no FROM "tblNutrients" WHERE nutrient_name = 'Vitamin A, IU') AS T2
		ON T1.ndb_no = T2.ndb_no
		ORDER BY T1.ndb_no
	)
);

-- # II. create VIEW of FoodProducts with ONLY beta carotene 
-- ## NOTE: UNSED BECAUSE NOT INTERESTING
CREATE MATERIALIZED VIEW vw_1_FoodsWithBCaroteneAndVitA AS(
	SELECT * FROM "tblBrandedFoodProducts"
	WHERE ndb_number IN(
		SELECT T1.ndb_no IN
			(SELECT ndb_no FROM "tblNutrients" WHERE nutrient_name = 'Carotene, beta') AS T1
		INNER JOIN
			(SELECT ndb_no FROM "tblNutrients" WHERE nutrient_name != 'Vitamin A, IU') AS T2
		ON T1.ndb_no = T2.ndb_no
		ORDER BY T1.ndb_no
	)
);

-- # III. create VIEW of FoodProducts with ONLY Vitamin A
-- ## NOTE: UNSED BECAUSE NOT INTERESTING
CREATE MATERIALIZED VIEW vwFoodsWithBCaroteneAndVitA AS(
	SELECT * FROM "tblBrandedFoodProducts"
	WHERE ndb_number IN(
		SELECT T1.ndb_no IN
			(SELECT ndb_no FROM "tblNutrients" WHERE nutrient_name != 'Carotene, beta') AS T1
		INNER JOIN
			(SELECT ndb_no FROM "tblNutrients" WHERE nutrient_name = 'Vitamin A, IU') AS T2
		ON T1.ndb_no = T2.ndb_no
		ORDER BY T1.ndb_no
	)
);

-- # IV. create VIEW of Nutrients with both Beta Carotene and Vitamin A
CREATE MATERIALIZED VIEW duke."vw_2_NutrientsBCVitA" AS (
 SELECT "tblNutrients".ndb_no,
    "vwFoodsWithBCaroteneAndVitA".long_name,
    "tblNutrients".nutrient_code,
    "tblNutrients".nutrient_name,
    "tblNutrients".output_value,
    "tblNutrients".output_uom,
    "vwFoodsWithBCaroteneAndVitA".manufacturer,
    "vwFoodsWithBCaroteneAndVitA".ingredients_english
   FROM duke."tblNutrients",
    duke."vwFoodsWithBCaroteneAndVitA"
  WHERE "tblNutrients".ndb_no = "vwFoodsWithBCaroteneAndVitA".ndb_number
);

-- # V. create VIEW of all foods GROUP BY manufactuers
CREATE MATERIALIZED VIEW "vw_3_CountsManufactuers" AS (
 SELECT "tblBrandedFoods".manufacturer,
    count("tblBrandedFoods".manufacturer) AS count
   FROM duke."tblBrandedFoods"
  GROUP BY "tblBrandedFoods".manufacturer
  ORDER BY (count("tblBrandedFoods".manufacturer)) DESC
);

-- # VI. create VIEW that shows all colon cancer risk foods
CREATE MATERIALIZED VIEW "vw_4_ColonCaRiskFoods" AS (
	SELECT "index",
			ndb_number,
			long_name,
			data_source,
			gtin_upc,
			manufacturer,
			date_modified,
			date_available,
			ingredients_english
	FROM "tblBrandedFoods", "brgCancer2FoodsToAvoid"
	WHERE "ingredients_english" LIKE '%'|| "FoodsToAvoid" ||'%' --concatenates all values in colFoodsToAvoid
);




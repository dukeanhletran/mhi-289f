# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 12:21:11 2019

This script is designed to:


@author: dletran
"""
import os
import pandas as pd
import sqlalchemy as sa
import tarfile as tf

#connect to database
## db: hello_world
## pw: <YOU-NEED-TO-CHANGE-THIS>

engine = sa.create_engine('postgresql://postgres:password@localhost:5432/hello_world')

#tarball name
fooddb = 'foodb_2017_06_29_csv'

#extract tarball -- useful in Windows or non-unix environment
tarball = tf.open(fooddb + '.tar.gz')
tarball.extractall()
tarball.close()

# grab list of files
ls_files = [x for x in os.listdir(fooddb) if ('._' not in x) and ('DS' not in x)]

# iterate through ls of csv files; load into database
for csv_file_name in ls_files:
    df = None; df = pd.DataFrame() #flush
    try:# regular UTF-8 encoding
        df = pd.read_csv(fooddb + '/' + csv_file_name) 
    except UnicodeDecodeError: # not UTF-8 encoding for some reason
        df = pd.read_csv(fooddb + '/' + csv_file_name, encoding="ISO-8859-1") 
    except Exception as e: # print errors
        print("Unexpected Error;" + csv_file_name)
        print(e)
        continue
    df.columns = [cols.lower() for cols in df.columns] #postgres doesn't like capitals or spaces
    df.to_sql(csv_file_name, engine)

# Fin

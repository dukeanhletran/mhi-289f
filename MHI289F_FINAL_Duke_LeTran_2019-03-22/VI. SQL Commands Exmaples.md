2019-01-26-SQL Commands Exmaples.md

# I. Joins

## A. Terminology

This is how to create a new view from the intersection or union of two tables.

### Left vs. Right
* **Left** - uses the first table as a reference
* **Right** - uses the second table as a reference

### Inner vs. Outer
| SQL   | Math          |Probability | Programming (C) |
|-------|---------------|------------|-----------------|
| Inner | Intersection  | AND        | &&              |
| Outer | Union         | OR         | ⎮⎮              |

![venn](https://bitbucket.org/dukeanhletran/mhi-289f/raw/79235c5acb0585b3d472d3106991d5ead771880e/img/intersection-union.png)

## A. Left Join

This is joining two tables which will be respectively named T1 and T2.

```{SQL}
SELECT T1.CallType, T2.CallDesc
FROM T1 LEFT JOIN T2
ON T1.CallTypeID = T2.ValueID
```
^ Only in-class example so far
* TO-DO: More examples to comes

# Links
* https://stackoverflow.com/questions/38549/what-is-the-difference-between-inner-join-and-outer-join
* https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/comparison_with_sql.html
* https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
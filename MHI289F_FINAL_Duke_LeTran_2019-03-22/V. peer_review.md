# Chris Fencl
## Quality of Work (1/1)
   Chris Fencl created the "Unbranded" foods which parallels my USDA-derived "BrandedFoodsProducts"; his table and mine connected together using the Nutrients, which allowed for some interesting joins.

## Interaction Score (1/1)
  Chris Fencl was incredibly nice, patient, helpful, and proactive about seeking help; working with Chris was a geniune pleasure, and his personableness allowed for an incredibly productive environment.

## Overall Contribution (1/1)
  Chris Fencl's work was instrumental in providing our overall project with a perspective on unbranded food -- a feat that is novel and new; his work added a parallel to mine (Branded Foods), which furthered the depth of the our collaborative effort as a whole.

## Total
(3/3)


# Chris Lucas
## Quality of Work (1/1)
  Chris Lucas created the table cancer with the ID type, which is the single table that connected everyone's table to the mimic database -- this allows for some genuinely interesting opportunities.

## Interaction Score (1/1)
  Chris Lucas showed great leadership in unifying the class; he was the glue that tied all our relations (tables) together.

## Overall Contribution (1/1)
  Not only did Chris Lucas generate his own tables, his leadership was instrumental in tying everyone's tables together and ultimately to the mimic database.

## Total
(3/3)
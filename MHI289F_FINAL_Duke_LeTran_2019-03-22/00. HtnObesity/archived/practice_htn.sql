﻿SELECT DISTINCT expire_flag, COUNT(expire_flag)
FROM patients P1 LEFT JOIN diagnoses_icd D1
ON P1.subject_id = D1.subject_id
WHERE icd9_code IN
	(SELECT icd9_code -- filter for ICD9 codes of hypertension
	FROM d_icd_diagnoses
	WHERE long_title LIKE '%hypertension%' AND 
	long_title NOT LIKE '%obesity%')
GROUP BY expire_flag

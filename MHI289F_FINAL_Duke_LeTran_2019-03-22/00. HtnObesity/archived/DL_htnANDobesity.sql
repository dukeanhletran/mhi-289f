﻿SELECT * FROM
	(SELECT T1.subject_id, T1.gender, T1.icd9_code, d_icd_diagnoses.long_title FROM 
		(SELECT DISTINCT P1.subject_id, gender, icd9_code, expire_flag, dob
		FROM patients P1 
		LEFT JOIN diagnoses_icd D1
		ON P1.subject_id = D1.subject_id 
		WHERE icd9_code IN
			(SELECT icd9_code -- filter for ICD9 codes of hypertension
			FROM d_icd_diagnoses
			WHERE long_title LIKE '%hypertension%')
		ORDER BY subject_id) AS T1
	LEFT JOIN d_icd_diagnoses
	ON T1.icd9_code = d_icd_diagnoses.icd9_code) AS tblHypertension
INNER JOIN
	(SELECT T1.subject_id, T1.gender, T1.icd9_code, d_icd_diagnoses.long_title FROM 
		(SELECT DISTINCT P1.subject_id, gender, icd9_code, expire_flag, dob
		FROM patients P1 
		LEFT JOIN diagnoses_icd D1
		ON P1.subject_id = D1.subject_id 
		WHERE icd9_code IN
			(SELECT icd9_code -- filter for ICD9 codes of hypertension
			FROM d_icd_diagnoses
			WHERE long_title LIKE '%obesity%')
		ORDER BY subject_id) AS T1
	LEFT JOIN d_icd_diagnoses
	ON T1.icd9_code = d_icd_diagnoses.icd9_code) AS tblObesity
ON tblHypertension.subject_id = tblObesity.subject_id



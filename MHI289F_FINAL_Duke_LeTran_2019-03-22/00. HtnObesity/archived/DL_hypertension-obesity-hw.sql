-- Title:hypertension-obesity-hw.sql
-- Author: Duke LeTran
-- Date: 2019-03-18

-- # HYPERTENSION ONLY
CREATE MATERIALIZED VIEW duke."hypertension_only" AS
SELECT subject_id FROM mimiciii.patients WHERE subject_id IN ( --drop duplicates hack
SELECT 
  patients.subject_id
--  diagnoses_icd.icd9_code, 
--  d_icd_diagnoses.long_title, 
--  d_icd_diagnoses.short_title
FROM 
  mimiciii.d_icd_diagnoses, 
  mimiciii.diagnoses_icd, 
  mimiciii.patients
WHERE 
  diagnoses_icd.icd9_code = d_icd_diagnoses.icd9_code AND
  patients.subject_id = diagnoses_icd.subject_id AND
  d_icd_diagnoses.long_title LIKE '%hypertension%' AND --include hypertension
  d_icd_diagnoses.long_title NOT LIKE '%obesity%') --exclude obesity

-- # OBESITY ONLY
CREATE MATERIALIZED VIEW duke."obesity_only" AS
SELECT subject_id FROM mimiciii.patients WHERE subject_id IN ( --drop duplicates hack
SELECT 
  patients.subject_id
--  diagnoses_icd.icd9_code, 
--  d_icd_diagnoses.long_title, 
--  d_icd_diagnoses.short_title
FROM 
  mimiciii.d_icd_diagnoses, 
  mimiciii.diagnoses_icd, 
  mimiciii.patients
WHERE 
  diagnoses_icd.icd9_code = d_icd_diagnoses.icd9_code AND
  patients.subject_id = diagnoses_icd.subject_id AND
  d_icd_diagnoses.long_title LIKE '%obesity%' AND --include obesity
  d_icd_diagnoses.long_title NOT LIKE '%hypertension%') --exclude hypertension

-- # HYPERTENSION AND OBESITY
CREATE MATERIALIZED VIEW duke."hypertension_AND_obesity" AS
SELECT subject_id FROM mimiciii.patients WHERE subject_id IN ( --drop duplicates hack
(SELECT 
  patients.subject_id
--  diagnoses_icd.icd9_code, 
--  d_icd_diagnoses.long_title, 
--  d_icd_diagnoses.short_title
FROM 
  mimiciii.d_icd_diagnoses, 
  mimiciii.diagnoses_icd, 
  mimiciii.patients
WHERE 
  diagnoses_icd.icd9_code = d_icd_diagnoses.icd9_code AND
  patients.subject_id = diagnoses_icd.subject_id AND
  d_icd_diagnoses.long_title LIKE '%obesity%') --include obesity
INTERSECT
(SELECT 
  patients.subject_id
--  diagnoses_icd.icd9_code, 
--  d_icd_diagnoses.long_title, 
--  d_icd_diagnoses.short_title
FROM 
  mimiciii.d_icd_diagnoses, 
  mimiciii.diagnoses_icd, 
  mimiciii.patients
WHERE 
  diagnoses_icd.icd9_code = d_icd_diagnoses.icd9_code AND
  patients.subject_id = diagnoses_icd.subject_id AND
  d_icd_diagnoses.long_title LIKE '%hypertension%')) --include hypertension

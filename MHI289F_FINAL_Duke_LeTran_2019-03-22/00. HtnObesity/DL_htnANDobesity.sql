﻿-- Author: Duke LeTran
-- Date: 2019-03-19
-- Description: Filters for Pts diagnosed with both obesity and hypertension
CREATE MATERIALIZED VIEW duke.vwObesityAndHtn AS
(SELECT tblH.subject_id, tblH.gender, tblH.icd9_code, tblH.long_title, tblO.icd9_code AS icd9_code2, tblO.long_title AS long_title2 FROM
	(SELECT T1.subject_id, T1.gender, T1.icd9_code, d_icd_diagnoses.long_title FROM 
		(SELECT DISTINCT P1.subject_id, P1.gender, D1.icd9_code, P1.expire_flag, P1.dob
		FROM patients P1 
		LEFT JOIN diagnoses_icd D1
		ON P1.subject_id = D1.subject_id 
		WHERE D1.icd9_code IN
			(SELECT d_icd_diagnoses.icd9_code -- filter for ICD9 codes of hypertension
			FROM d_icd_diagnoses
			WHERE long_title LIKE '%hypertension%')
		ORDER BY subject_id) AS T1
	LEFT JOIN d_icd_diagnoses
	ON T1.icd9_code = d_icd_diagnoses.icd9_code) AS tblH
INNER JOIN
	(SELECT T1.subject_id, T1.gender, T1.icd9_code, d_icd_diagnoses.long_title FROM 
		(SELECT DISTINCT P1.subject_id, P1.gender, D1.icd9_code, P1.expire_flag, P1.dob
		FROM patients P1 
		LEFT JOIN diagnoses_icd D1
		ON P1.subject_id = D1.subject_id 
		WHERE D1.icd9_code IN
			(SELECT d_icd_diagnoses.icd9_code -- filter for ICD9 codes of hypertension
			FROM d_icd_diagnoses
			WHERE long_title LIKE '%obesity%')
		ORDER BY subject_id) AS T1
	LEFT JOIN d_icd_diagnoses
	ON T1.icd9_code = d_icd_diagnoses.icd9_code) AS tblO
ON tblH.subject_id = tblO.subject_id)
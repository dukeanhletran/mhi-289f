# Notes
I added to the existing ontology FoodOn. 
## RED MEAT
Red Meat is a very vague term, and even the USDA is inconsistent with their definition from factsheet to factsheet. The definitions here are defined as such through the examples provided by the WHO. This is by no means an exhaustive list.
## PROCESSED MEAT
This is also a very vague term that tends to have colloquial meaning. The listed items from hot dogs to ham is also defined by the examples provided by the WHO. Again, this is by no means an exhaustive list.
"" - column names
'' - values inside 

SELECT "Value", "CallTypeDesc"
	FROM public. "PoliceCallTypes"
	# This will show you all the types

SELECT "Value", "CallTypeDesc"
	FROM public. "PoliceCallTypes"
	where "PoliceCallTypes.CallTypeDesc" like 'rob%' #'like' is similar to 'equal' but it's a fuzzy search



# % - wildcard
# like - fuzzy search
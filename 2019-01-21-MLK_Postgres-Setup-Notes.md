# Database Notes: How to set-up postgres properly on Fedora 29

## A. Installation

### 1. PostGres (and PgAdmin IV)

```{bash}
sudo dnf install -y postgres # i think
```
<IN-PROGRESS> need to go back to update

### 2. PgAdmin III

#### pgadminpack
* Docs: https://www.pgadmin.org/docs/pgadmin3/1.22/extend.html
* Forums:https://www.postgresql-archive.org/The-server-lacks-instrumentation-functions-td2082259.html

### 3. Reference to Postgres Fedora Yum Repositories
* https://download.postgresql.org/pub/repos/yum/10/fedora/

## B. Basics

How to change user,e.g., from 'dukeletran' to postgres' in bash terminal -- allows you to start the postgres server as the default superuser 'postgres'.


```{bash}
sudo -u postgres -i
```
This starts the database server CLI

```{bash}
psql
```

This is the CLI firewall program.


```{bash}
firewalld
```

This is the GUI firewall program

```{bash}
firewall-config
```


## C. Common SQL Commands
CLI equivalent programs that essentially runs ``` psql -c "SQL Command"```
```
createuser <user>
psql -c "CREATE USER <user>" 
dropuser <user>
psql -c "DROP USER <user>"
```
list all the users in the database

```{bash}
psql -c "\du"

```
lists all the databases

```{bash}
psql -c "\l"
psql -c "\list"
```

list all tables

```{bash}
psql -c "\dt"
```



## C. Configuration Files
```{bash}
psql -c "SHOW config_file"
```
* /var/lib/pgsql/data/postgresql.conf
* /var/lib/pgsql/data/pg_hba.conf

## D. Connecting PgAdminIII to postgres server
* http://suite.opengeo.org/docs/latest/dataadmin/pgGettingStarted/pgadmin.html

## E. How to run an sql file
* Note, obviously be wary of running random sql scripts online
* https://stackoverflow.com/questions/9736085/run-a-postgresql-sql-file-using-command-line-arguments
* Note, the ```-a``` flag is the short version of ```--echo-all``` and is not necessary; it just prints all nonempty input lines to standard output as they are read. See psql manual.

This is command below runs on a local postgres server.

```{bash}
psql -U username -d myDataBase -a -f myInsertFile
```

if the db is remote.

```{bash}
psql -h host -U username -d myDataBase -a -f myInsertFile
```


# Links
* https://unix.stackexchange.com/questions/201666/command-to-list-postgresql-user-accounts
* https://dba.stackexchange.com/questions/1285/how-do-i-list-all-databases-and-tables-using-psql
* http://suite.opengeo.org/docs/latest/dataadmin/pgGettingStarted/pgadmin.html
* http://www.postgresqltutorial.com/psql-commands/
* https://fedoraproject.org/wiki/PostgreSQL
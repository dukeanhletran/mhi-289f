

# I. Introduction
# MIMIC
* no numbers in user_id

# II. Review
## A. Types of SQL
* DDL
* DML
* DCL
* TCL 

See Table, Going into the links


## B. RDBMS
* Normalizing - achieving consistency with your data # not the same
	- Referrential Integreit

* Denormalize for analytical analysis - to seepd up

## C. Normalization process
* no item will be unnecessarily stored
* non-prime atrributes in the table are dependent on the primary key
	- non-prime attribute are associated with that single indentifyer that is not repeated in the table,ie, primary key

....therefore, each table is void of anomlies


## D. Normalization Goal
* works one relation at a time
* start by... identyfing dpeendencies (is the table dependent on something else)
* .... if the table is dependent on something else, we can break them into a different set of relations

WHAT We're going to do... break the up into separate tables os that it is not redundant.

## . EExample of normaliztion

1st normal form (1NF) > 2nd normal form (2NF) > 3NF > etc.

* repeating roup: group of mutiple entries of same type can exist for anysinge keys
* elininated repaeting group


##. Defintions

[plantID][genus][species][common_namme][uses]
1; Dodonaea viscosa; akaeak; soil stability, hedging, shelter 


[plantID][genus][species][common_namme][use1][use2][use3]
1; Dodonaea viscosa; akaeak;[hedging][shelter]soil[stability]

^this one is better

CSV - comma delimited data
TSV - tab delimited data


1st normal form - split the tables if tehr is form
2nd normal form - if we nave all teh valeues of the


## From last time, written notes

```{SQL}
-- left join
SELECT T1.CallType, T2.CallDesc.
	from T1 LEFT JOIN T2
	on T1.CallTypeID = T2.ValueID
```

This prioritizes

## Data 
* 100k records is not that much
* 1 million, you can do some pretty cool stuff
* 
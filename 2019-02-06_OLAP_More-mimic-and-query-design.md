2019-02-06_OLAP_More-mimic-and-query-design.md

# 00. Introduction


# I. Business Intelligence (BI)

* 5 to 6 years ago used to be all the rage, before AI
* BI was based around OLAP
* capture, collecte, integrate, and store data for analysis
* build a dashboard (present info) for business decision making
* converts "data into information, information into knowledge, knowledge to wisdom"

## DIKW Summary by professor
* data is just noise (results of experiment, etc.)
* Information is data in context
* Knowlege is information that is information actionable
* Wisdom is seeing trend of results from a lot of accumulated knowledge

* Dashboards annd OLAP
* we can query stuff realtime now
* stock markets are based off quarterly reports (culturally and politically)
* PNL for last quarter; vs real time look at how we're doing

## OLTP 
* a lot of writes (updates, inserts, deletes)
* exchanging money at the bank
* updating a patient chart
* ordering a script, and having it delivered to Pharmacy
* fulfilling that script -- completeing a workflow
* other workflow -- maybe it's about whether pt is compliant?


## OLAP
* analytical processing
* database functional design
* OLAP systems that query OLTP systems in real time
* supports data mining and analytics
* Examples: bed occupancy rates by dept by year
* Examples: provider contact time by Tx, encounters per day, cost of care analyses

## II. Four aspects on business intelligence (analytics)
* Extract, Transform, Load -- filter out drugs from entire label of drugs, etc.

## Practices to manage data 
* MDM - 
* governance - who has access to specific databases; permissions
* Key performance indicators (KPI) - have we increased cost per encounter; have we improved occupancy


## Practices to Data
* Data visualization - abstracting data to provide information in a visual format

## OLAP
* google data studio
* dashboards are often actual applications, often written in other code like Java, etc.

## Key differences between OLTP vs. OLAP
* OLTP, "operational", normalized, relational, super detailed, real time, 
* OLAP,  "decision-support", non-normalized,  multidimentionalm summary, snapshots

## physical data model; 
* how data is physically stored on the disk
* think about taking a card in/out of a ordered/unordered deck
* 
* index -- suit, then number (card is indexed by)

### Ordered vs unordered
* Ordered - read is fast, write is low
* Unordered - read is slow, write is fast

### Examples
* what if you need to update 500 records, remove indices to make read faster; add indices back in
* index is what you're searching on

```{SQL}
ALTER TABLE, remove index # DML
 # DML
# add index back in
```


# III. Star Schemas

* It means I've got 1 or 2 tables in teh center, that links out to other tables, that links out to other tables
* "Snowflake schema"

## componentsN
1. Facts - numeric values that represents a specific business aspect
2. Dimensions
3, Attributes -how do I want to slice and dice these dimensions
4. heirarchy - drill down

## Additional notes
* subject to primary and foreign key constraints
* primary key of a fact table
	* easier to take a look

## A. Facts

## B. Dimensions
* various ways which we can see the patient days, in whihc buckets

## HEalth care example
* episode of care fact -- may have pulled them from multiple tables and created a fact table
* a person can be in here a lot, for different episodes
* a composite of many IDs can be the primary key
* you can link to other dimensions -- ICD9, therpaist dimension, race, gender, etc.

### Decision support
* You can say... how did all patients seen by John change in body dimensions
* How does John compare to Mary?
* etc questions


2019-01-16 

* datbase systesms, functions
* learn sql

# I. Stages of Data Model
* reporting

Entity - Car
Attribute
* e.g. Colors of Car, Red or black
* Entity Characteristic

## Naming Convention
name_convention - use CamelCase
* tblCar

### Different Table structures

#### attributes examples
* CarID | Make | Model | Color
* CarID | Make | isRed | isWhite | isBlue

## Check Anki cards
* mostly  review

# II. Learn SQL

## A. Four Categories of SQL

### DDL
Data Definition Language
* CREATE
* PUT IN NAMES OF COLUMNS
* PUT IN DATA tYPES (charcter, binary, numerical)
Eg. Alter Table

### DML
Data manipulation language
Definitoin: to get data in and out of it; to retrieve, store, modify, delete, insert and update

### DCL
Data Control Lanugage
* create roles and permissions
#### referrential integrity 
```see paper notes```
* cascading updates; cascading deletes
* if you delete an ID in one table, does it "cascade" and delete an ID in the next table?
* or does it just refer to null
e.g. Alter default privileges

### TCL
Transacctional Control  Language
* the bank thing, all or nothing


## B. More indepth

### i. DML
* Select, Select info
* Insert
* Update
* Delete

### ii. DDL
* Create Table/View #also procedures, but we won't go into it for now
    View - see two tables 
* Alter Table/View/Column # add a new column


* 



### TCL
Transactional Control Language


MYSQL
```{MYSQL}
SHOW TABLES
```

```{postgre}
SELECT * FROM pg_catlog.pg_tables;
```


pg_catalog - a datbase that stores
pg_tables - 



DDL, delete
-drop table, drop constraint

DCL
-drop rule
# rule - permission for users


# Other
* IP Address - 256 octet
127.0.0.1:PORT
.conf

## Pacifico
* co-op off campus, failed, low-income housing, 
* county wants it to turn into 



------------------------------------

n